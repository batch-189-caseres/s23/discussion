/*
	OBJECTS
		An object is a data type that is used to represent real world objects. It is also a collection of related data and/or
		functionalities
		
		Syntax:
			let objectName = {
				keyA: valueA'
				keyB: valueB
			}
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}

let cellphone1 = {
	name: "Nokia 3211",
	manufactureDate: 2000
}

console.log("Result from creating objects");
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a CONSTRUCTOR FUNCTION
/*
	Creates a reusable function to create a several objects that have the same data structure. 
	This is useful for creating multiple instances/copies of an object.
	
	Syntax:
		function ObjectName(valueA, valueB) {
			this.keyA = valueA,
			this.keyB = valueB
		}

*/

function Laptop (name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate
}

let laptop = new Laptop("Lenovo", 2008);
console.log("result of creating objects using object constructor ");
console.log(laptop);

let myLaptop = new Laptop("MacBook Air", 2020);
console.log(myLaptop);

let oldLaptop = new Laptop("Portal R2E CCMC", 1980);
console.log(oldLaptop);

// Creating an EMPTY OBJECTS
let computer = {} // Most used!
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

// Accessing Object Property
// Using the Dot Notation
console.log("Result from dot notation: " + myLaptop.name); // Most used!

// Using the Square Bracket Notation
// We can use space only in this notation
console.log("Result from square bracket notation: " + myLaptop['name']);

// Accessing Array Objects
let array = [laptop, myLaptop];
console.log(array[0]["name"]);

console.log(array[0].name);

// Initializing / Adding / Deleting / Reassgning Object Properties
let car = {}
console.log(car);

car.name = "Honda Civic";
console.log("Result from adding property using dot notation");
console.log(car);

car["manufacture date"] = 2019;// Special feature of Square Bracket Notation
console.log("Result from adding property using square bracket notation");
console.log(car);

// Deleting Object Properties
delete car["manufacture date"];
console.log("Result from deleting object properties: ");
console.log(car);

// Reassigning Object Properties
car.name = "Tesla";
console.log("Result from reassigning property: ");
console.log(car);

// Object Methods
/*
	A method is a function which is a property of an object. They are also functions
	and one of the key differences the have is that methods are functions related to a 
	specific object.

*/

let person = {
	name: "John",
	talk: function () {
		console.log("Hello! My name is " + this.name);
	}
}
console.log(person)
console.log("Result from object methods: ");
person.talk()

// Adding function to object
person.walk = function() {
	console.log(this.name + " walked 25 steps forward.");
}

person.walk()

let friend = {
	firstName: "Nehemiah",
	lastName: "Ellorico",
	adress: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["nejellorico@mail.com", "nej123@mail.com"],
	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName);
	}
}
friend.introduce();

// Real World Application Objects
/*
	Scenario:
		1. We would like to create a game that would have several pokemon
		interact with each other.
		2. Every pokemon would have the same set of stats.

*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to targetPokemon");
	},
	faint: function() {
		console.log("Pokemon fainted");
	}
}
console.log(myPokemon);

// Creating an object constructor
function Pokemon (name,level) {
	// properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
	},
	this.faint = function() {
		console.log(this.name + " fainted.");
	}
}

let pikachu = new Pokemon ("Pikachu", 16);
let squirtle = new Pokemon("Squirtle", 8);

console.log(pikachu);
console.log(squirtle);

pikachu.tackle(squirtle);
pikachu.tackle(squirtle);

// ACTIVITY:
function Pokemon (name,level) {
	// properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;



	// Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		let presentHealth = target.health - this.attack;
		console.log(target.name + "'s health is now reduced to " + presentHealth);
		target.health = presentHealth
			if(presentHealth <= 5){
		this.faint(target.name)
	}
	},
	this.faint = function(target) {
		console.log(target + " fainted.");
	}
}

let charizard = new Pokemon ("Charizard", 16);
let mewto = new Pokemon("Mewto", 8);

mewto.tackle(charizard)
mewto.tackle(charizard)
mewto.tackle(charizard)
mewto.tackle(charizard)
mewto.tackle(charizard)
mewto.tackle(charizard)